#/user/bin/env python
print "Welcome to the AIMS module"
def std(t):
        try:
                import math
                import numpy as np
                t=np.array(t)
                n=len(t)
                m=float(sum(t))/n
                t=t-m
                t=t**2
                x=math.sqrt(sum(t)/n)
                return x
        except ZeroDivisionError:
                return 0
